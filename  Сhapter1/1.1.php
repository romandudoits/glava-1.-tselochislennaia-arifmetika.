<?php
$n = 1234561;


function quantityNumber ($n)
{
    $count = 0;
    while ($n > 1) {
        $num = $n % 10;
        $n = $n / 10;
        if ($num < 5) {
            $count += 1;
        }
    }
    return $count;
}

echo quantityNumber($n);