<?php

$n = 10000;

for ($i = 1; $i < $n; $i++)
{
    if (isPalindrome($i))
    {
        $palindrome = pow($i, 2);

        if (isPalindrome($palindrome))
        {
            echo $i . ' число полиндром' . '<br>';
        }
    }

}

function isPalindrome ($i)
{
    $number = $i;
    $flag = 0;
    $result = false;
    while ($number >= 1)
    {
        $numb = $flag * 10 + $number % 10;
        $number = $number / 10;
        $flag = $numb;
    }
    if ($i == $flag)
    {
        $result = true;
    }
    return $result;
}
