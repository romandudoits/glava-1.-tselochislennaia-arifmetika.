<?php

$n = 12436;
$flag = 'Нет';

while ($n >= 1)
{
    $numb = $n % 10;
    $n = $n / 10;
    $q = $n;
    while ($q >= 1)
    {
        $numb1 = $q % 10;
        $q = $q / 10;
        if ($numb == $numb1)
        {
            $flag = 'Да';
            break;
        }
    }
}
echo $flag;