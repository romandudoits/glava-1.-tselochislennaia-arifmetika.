<?php

$m = 2000;

for ($i = 1; $i <= $m; $i++)
{
    $n = $i;
    $flag = 0;
    while ($n >= 1)
    {
        $numb = $n % 10;
        $n = $n / 10;
        $flag += $numb;
    }

    if ($i % $flag == 0)
    {
        echo $i . '<br>';
    }
}
